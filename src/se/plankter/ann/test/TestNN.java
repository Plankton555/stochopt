package se.plankter.ann.test;

import se.plankter.ann.INeuralNetwork;
import se.plankter.ann.NeuralNetwork;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Plankton on 2014-10-09.
 */
public class TestNN {

    public static void main(String[] args) {

        System.out.println("Generating training data");
        // AND
        List<double[]> trainingDataAND = new ArrayList<double[]>();
        trainingDataAND.add(new double[]{0, 0, 0});
        trainingDataAND.add(new double[]{0, 1, 0});
        trainingDataAND.add(new double[]{1, 0, 0});
        trainingDataAND.add(new double[]{1, 1, 1});

        // AND
        List<double[]> trainingDataXOR = new ArrayList<double[]>();
        trainingDataXOR.add(new double[]{0, 0, 0});
        trainingDataXOR.add(new double[]{0, 1, 1});
        trainingDataXOR.add(new double[]{1, 0, 1});
        trainingDataXOR.add(new double[]{1, 1, 0});

        printData(trainingDataAND);
        System.out.println("\n");
        printData(trainingDataXOR);
        System.out.println("\n");


        System.out.println("Creating network");
        INeuralNetwork ann = new NeuralNetwork();
        ann.createNetworkStructure(new int[]{2, 2, 1});

        System.out.println("Network created, starting training loop");
        int nrIterations = 10;
        for (int i = 0; i < nrIterations; i++) {
            // pick a data tuple to train with

            //feed the network with it

            //backpropagate the error

            //monitor the performance
            System.out.println("loop, iteration " + (i+1));
        }

    }

    private static void printData(List<double[]> data) {
        for (int i=0; i<data.size(); i++)
        {
            for (int j=0;j<data.get(i).length; j++)
            {
                System.out.print("" + data.get(i)[j] + " \t ");
            }
            System.out.println();
        }
    }
}

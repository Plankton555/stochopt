package se.plankter.ann;

/**
 * Created by Plankton on 2014-10-09.
 */
public interface INeuralNetwork {
    void createNetworkStructure(int[] layers);
    NNOutput feedNetwork(NNInput input);
    void backPropagate();
}

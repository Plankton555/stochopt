package se.plankter.ann;

import org.la4j.matrix.dense.Basic2DMatrix;
import org.la4j.matrix.functor.MatrixProcedure;

import java.util.Random;

/**
 * Created by Plankton on 2014-10-09.
 */
public class NeuralNetwork implements INeuralNetwork {

    private Basic2DMatrix[] allWeights;
    private Random rand;

    public NeuralNetwork() {
        rand = new Random();
    }

    @Override
    public void createNetworkStructure(int[] layers) {
        if (layers.length < 2) {
            throw new IllegalArgumentException("The network must have at least 2 layers (input and output)");
        }
        int nrLayers = layers.length; // includes input, hidden and output
        allWeights = new Basic2DMatrix[nrLayers-1];
        for (int i = 0; i < nrLayers - 1; i++) {
            // includes extra set of weights for bias/threshold units
            allWeights[i] = initializeWeights(layers[i+1], layers[i]+1);
        }
        System.out.println("weights[0]:");
        System.out.println(allWeights[0].toString());
    }

    private Basic2DMatrix initializeWeights(int nrNeuronsTo, int nrNeuronsFrom) {
        Basic2DMatrix weights = new Basic2DMatrix(nrNeuronsTo, nrNeuronsFrom);
        weights.each(new RandomWeightInit());
        System.out.println("weights:");
        System.out.println(weights.toString());
        return weights;
    }

    @Override
    public NNOutput feedNetwork(NNInput input) {
        return null;
    }

    @Override
    public void backPropagate() {

    }

    /**
     * A MatrixProcedure for setting the elements to uniformly random numbers in interval [initMin, initMax]
     */
    private class RandomWeightInit implements MatrixProcedure {
        double initMin = -0.2;
        double initMax = 0.2;
        @Override
        public void apply(int i, int i2, double v) {
            double r = rand.nextDouble()*(initMax-initMin)+initMin;
            v = r;
            System.out.println("v = " + v);
        }
    }
}

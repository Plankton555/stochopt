package se.plankter.genalg;

/**
 * 
 * @author Bjorn P Mattsson
 * 
 */
public interface IChromosome {

	/**
	 * Returns a number of (normally two) chromosomes that are the result of a
	 * crossover between the two specified chromosomes.
	 * 
	 * @param c1
	 *            Chromosome 1
	 * @param c2
	 *            Chromosome 2
	 * @return An array containing a number of chromosomes that are the result
	 *         of a crossover
	 */
	IChromosome[] crossover(IChromosome c1, IChromosome c2);

	/**
	 * @return The number of genes in the chromosome
	 */
	int getChromosomeLength();

	/**
	 * Should return a new chromosome of the same type and with the same
	 * content. Must NOT reference the original chromosome, must be a new
	 * object.
	 * 
	 * @return A copy of the chromosome
	 */
	IChromosome getCopy();

	/**
	 * Performs mutation on this chromosome. Each gene will mutate with the
	 * given mutation probability.
	 * 
	 * @param mutationProbability
	 *            The probability that a gene in the chromosome will mutate
	 */
	void mutate(double mutationProbability);
}

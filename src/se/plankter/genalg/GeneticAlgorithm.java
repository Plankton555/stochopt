package se.plankter.genalg;

/**
 * 
 * @author Bjorn P Mattsson
 * 
 */
public class GeneticAlgorithm {

	private int populationSize;
	private int chromosomeLength;
	private double mutationRate;
	private int nrOfElitism;

	private IChromosome[] population;
	private IChromosome[] lastPopulation;
	private double[] fitness;
	private int bestIndividualIndex;
	private double bestIndividualFitness;

	private IChromosomeDecoder decoder;
	private ISelection selection;

	public GeneticAlgorithm(IChromosome[] population, ISelection selection,
			IChromosomeDecoder decoder) {
		this.populationSize = population.length;
		this.chromosomeLength = population[0].getChromosomeLength();

		if (populationSize < 1) {
			throw new IllegalArgumentException(
					"Population size must be larger than 0");
		}
		if (chromosomeLength < 1) {
			throw new IllegalArgumentException(
					"Chromosome length must be larger than 0");
		}

		this.mutationRate = 1.0 / chromosomeLength;
		this.nrOfElitism = 1;
		this.decoder = decoder;
		this.selection = selection;
		this.fitness = new double[populationSize];

		// Initialize population
		this.population = population;
		this.lastPopulation = population.clone();
		
		// TODO attribute to allow concurrency or not
	}

	/**
	 * @return The probability for a gene to mutate
	 */
	public double getMutationRate() {
		return mutationRate;
	}

	/**
	 * Sets the probability for a gene to mutate. By default set to 1.0 /
	 * chromosomeLength so that in average one gene mutates in each chromosome.
	 * The mutation rate must be a number between 0 and 1 (a probability).
	 * 
	 * @param mutationRate
	 *            Mutation rate
	 */
	public void setMutationRate(double mutationRate) {
		if (mutationRate < 0 || mutationRate > 1) {
			throw new IllegalArgumentException(
					"The mutation rate must be a probability (number between 0 and 1)");
		}
		this.mutationRate = mutationRate;
	}

	/**
	 * @return The amount of copies of the best chromosome that will be
	 *         transferred to the next generation without any changes
	 */
	public int getNrOfElitism() {
		return nrOfElitism;
	}

	/**
	 * Sets the number of elitism. By default set to 1. Must be a positive
	 * number smaller than the population size.
	 * 
	 * @param nrOfElitism
	 *            The amount of copies of the best chromosome that will be
	 *            transferred to the next generation without any changes
	 */
	public void setNrOfElitism(int nrOfElitism) {
		if (nrOfElitism < 0 || nrOfElitism > populationSize) {
			throw new IllegalArgumentException(
					"The number of elitism must be a positive number smaller than the population size");
		}
		this.nrOfElitism = nrOfElitism;
	}

	/**
	 * Makes a copy of the population using the IChromosome.getCopy() method.
	 * 
	 * @param origPopulation
	 *            The population to copy from.
	 * @param copyPopulation
	 *            The population to copy to.
	 */
	private void copyPopulationTo(IChromosome[] origPopulation,
			IChromosome[] copyPopulation) {
		if (origPopulation.length != copyPopulation.length) {
			throw new IllegalArgumentException("Different population lengths");
		}
		// TODO Should be able to be done concurrently with barriers
		for (int i = 0; i < origPopulation.length; i++) {
			copyPopulation[i] = origPopulation[i].getCopy();
		}
	}

	/**
	 * @return The currently best chromosome in the population
	 */
	public IChromosome getBestChromosome() {
		return lastPopulation[bestIndividualIndex];
	}

	/**
	 * @return The currently best fitness value in the population
	 */
	public double getBestFitness() {
		return bestIndividualFitness;
	}

	/**
	 * Runs one generation of the GA.
	 */
	public void runGeneration() {
		// Evaluate the individuals
		int maxFitnessIndex = -1;
		double maxFitness = Double.MIN_VALUE;
		for (int i = 0; i < populationSize; i++) {
			// TODO Should be able to be done concurrently with barriers
			fitness[i] = decoder.decodeChromosome(population[i]);
			if (fitness[i] > maxFitness) {
				maxFitnessIndex = i;
				maxFitness = fitness[i];
			}
		}
		bestIndividualIndex = maxFitnessIndex;
		bestIndividualFitness = maxFitness;

		// Form the next generation
		copyPopulationTo(population, lastPopulation);

		// Crossover
		int i1 = -1;
		int i2 = -1;
		// TODO Should be able to be done concurrently with barriers
		for (int i = 0; i < populationSize; i += 2) {
			i1 = selection.doSelection(fitness);
			i2 = selection.doSelection(fitness);
			IChromosome[] crossovered = lastPopulation[i1].crossover(
					lastPopulation[i1], lastPopulation[i2]);
			population[i] = crossovered[0];
			population[i + 1] = crossovered[1];
		}

		// Mutation
		for (int i = 0; i < populationSize; i++) {
			// TODO Should be able to be done concurrently with barriers
			population[i].mutate(mutationRate);
		}

		// Elitism
		for (int i = 0; i < nrOfElitism; i++) {
			population[i] = lastPopulation[bestIndividualIndex].getCopy();
		}
	}

	/**
	 * Runs the GA the specified amount of generations.
	 */
	public void runGeneration(int nrOfGenerations) {
		for (int i = 0; i < nrOfGenerations; i++) {
			runGeneration();
		}
	}

}

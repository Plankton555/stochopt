package se.plankter.genalg.test;

import se.plankter.genalg.GeneticAlgorithm;
import se.plankter.genalg.IChromosomeDecoder;
import se.plankter.genalg.ISelection;
import se.plankter.genalg.RealChromosome;
import se.plankter.genalg.TournamentSelection;

public class PermutationTest {
	
	private static double P_TOUR = 0.75;

	public static void main(String[] args) {
		// TODO Test permutations
		/*
		int populationSize = 20;
		int chromosomeLength = 2; // 2 variables
		RealChromosome[] population = new RealChromosome[populationSize];
		for (int i = 0; i < populationSize; i++) {
			population[i] = RealChromosome
					.getRandomChromosome(chromosomeLength);
		}

		ISelection selection = new TournamentSelection(P_TOUR);
		IChromosomeDecoder gpDecoder = new FctGoldPricDecoder();

		// Will test the Goldstein�Price function:
		// f(0,-1) = 3
		// -2 <= x,y >= 2

		GeneticAlgorithm ga = new GeneticAlgorithm(population, selection,
				gpDecoder);

		for (int i = 0; i < 100; i++) {
			ga.runGeneration();
			System.out.println("G: " + (i + 1) + "\tfitness: "
					+ ga.getBestFitness());
			System.out.println(ga.getBestChromosome().toString());
		}

		System.out.println("\nTest of Genetic Algorithm has been run");
		*/
	}
}
